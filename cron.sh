#!/bin/sh
cd $(dirname "$0")

SITE_DIR=site

# Retrieve website
wget -q -P "${SITE_DIR}" -m -nH "http://www.win.tue.nl/~pcuijper/QEES/"

# Add to Git
git add .
git commit -m "Update at $(date '+%F %T')" >/dev/null
git push

exit 0
