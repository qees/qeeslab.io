#!/usr/bin/env bash
set -euo pipefail

site_dir="site"
main_dir="~pcuijper/QEES"
pages_dir="public"
template="index.html"
links=""

mkdir "${pages_dir}"

# Get the relevant commits from the log
commits=$(git log --pretty=oneline | grep "Update at" | cut -d' ' -f1)

# Create the website for the relevant commit
for c in $commits
do
    # Checkout the relevant commit
    git checkout "$c"

    # Get the date of the commit
    date=$(git show -s --format=%ai "$c")

    # Copy the state of the site to the pages dir
    cp -a "${site_dir}" "${pages_dir}/${date}"

    # Add a link to the index
    links="${links}<li><a href=\"${date}/${main_dir}/\">${date}</a></li>"

    # Create a 'latest' symlink for the latest commit
    [ ! -e "${pages_dir}/latest" ] && ln -s "${date}" "${pages_dir}/latest"
done
git checkout master

# Create index file
sed "s|{{body}}|${links}|g" "${template}" > "${pages_dir}/index.html"

# Deduplicate files
rdfind -makesymlinks true "${pages_dir}"

# Make symlinks relative
find "${pages_dir}" -type d -exec symlinks -cs {} +
